
import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */
var data = {
  departaments: ['Neurology', 'Cardiology', 'Emergency'],
  nextActionTime: 900000,
  dataRefrashe: 3000,
  heartBeatRange: [60, 120],
  mode: 'light',
  alarmSound: true,
  darkMode: false,
  showRoomNr: true,
  showPatient: true,
  showFacilities: true,
  showAction: true,
  showMonioringData: true
}

if (typeof (Storage) !== 'undefined') {
  if (localStorage.getItem('hospital-settings-store') !== null) {
    data = JSON.parse(localStorage.getItem('hospital-settings-store'))
  }
}

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
    },
    state: data,
    getters: {
      GET_departaments: state => {
        return state.departaments
      },
      GET_alarmSound: state => {
        return state.alarmSound
      },
      refreshrate: state => {
        return state.dataRefrashe
      },
      GET_MODE: state => {
        return state.mode
      },
      GET_actionNextTime: state => {
        return state.nextActionTime
      },
      GET_showRoomNr: state => {
        return state.showRoomNr
      },
      GET_showPatient: state => {
        return state.showPatient
      },
      GET_showFacilities: state => {
        return state.showFacilities
      },
      GET_showMonioringData: state => {
        return state.showMonioringData
      },
      GET_showAction: state => {
        return state.showAction
      },
      GET_heartBeatRange: state => {
        return state.heartBeatRange
      }

    },
    mutations: {
      set_nextActionTime: async (state, actionTime) => {
        state.nextActionTime = parseInt(actionTime)
      },
      set_dataRefrashe: async (state, dataRefrashe) => {
        state.dataRefrashe = parseInt(dataRefrashe)
      },
      change_mode: (state) => {
        state.darkMode = !state.darkMode
        localStorage.setItem('hospital-settings-store', JSON.stringify(state))
      },
      toggleAlarm: (state) => {
        state.alarmSound = !state.alarmSound
        localStorage.setItem('hospital-settings-store', JSON.stringify(state))
      },
      showRoomNr: (state) => {
        state.showRoomNr = !state.showRoomNr
        localStorage.setItem('hospital-settings-store', JSON.stringify(state))
      },
      showPatient: (state) => {
        state.showPatient = !state.showPatient
        localStorage.setItem('hospital-settings-store', JSON.stringify(state))
      },
      showFacilities: (state) => {
        state.showFacilities = !state.showFacilities
        localStorage.setItem('hospital-settings-store', JSON.stringify(state))
      },
      showMonioringData: (state) => {
        state.showMonioringData = !state.showMonioringData
        localStorage.setItem('hospital-settings-store', JSON.stringify(state))
      },
      showAction: (state) => {
        state.showAction = !state.showAction
        localStorage.setItem('hospital-settings-store', JSON.stringify(state))
      },
      change_mode_name: async (state) => {
        if (state.darkMode === true) {
          state.mode = 'dark'
        } else {
          state.mode = 'light'
        }
      }
    },
    actions: {
    },
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
