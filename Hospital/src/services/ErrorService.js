import { Notify } from 'quasar'

export default {
  showAlertInfo (userMessage, pos = 'right') {
    Notify.create({
      color: 'positive',
      position: pos,
      message: userMessage,
      icon: 'info'
    })
  },
  showAlertError (userMessage, pos = 'right') {
    Notify.create({
      color: 'negative',
      position: pos,
      message: userMessage,
      icon: 'error'
    })
  },
  log (error) {
    console.log(error)
    this.$q.notify({
      message: 'Jim pinged you',
      color: 'purple',
      avatar: 'https://cdn.quasar.dev/img/boy-avatar.png'
    })
  }
}
