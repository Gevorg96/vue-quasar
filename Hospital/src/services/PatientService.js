import axios from 'axios'

const RESOURCE_PATH = 'http://localhost:3399/patient'

// this shows how you van provide a service as class
// an object literal syntax could also have been used

// axios is used here with 'then' callbacks, async/await syntax could also have been used

export default class PatientService {
  get () {
    // eslint-disable-next-line eqeqeq
    return axios.get(RESOURCE_PATH).then(result => result.data)
  }
  getById (id) {
    return axios.get(RESOURCE_PATH + '/' + id).then(result => result.data)
  }
  delete (room) {
    return axios.delete(RESOURCE_PATH + '/' + room.id)
  }
  add (room) {
    return axios.post(RESOURCE_PATH, room).then(result => result.data)
  }
  update (room) {
    return axios.put(RESOURCE_PATH + '/' + room.id, room)
  }
}
