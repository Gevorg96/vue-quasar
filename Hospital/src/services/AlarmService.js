export default {
  roomAlarm (room, patient, heartBeatRange, actionTime = 900000, track, alarmOnOf) {
    // sorting of actions
    patient.action.sort((a, b) => (new Date(b.time) - new Date(a.time)))
    // is heartbeat is low of high then the range !!!RED ALARM
    if (room.heartbeat < heartBeatRange[0] || room.heartbeat > heartBeatRange[1]) {
      if (alarmOnOf) {
        track.play()
      }
      return { backgroundColor: 'red',
        animationName: 'blinker',
        animationDuration: '1s',
        animationTimingFunction: 'ease-in',
        animationIterationCount: 'infinite'
      }
    }
    // Patient need help
    if (patient.alarm === true) {
      return { backgroundColor: 'red'
      }
    }
    // if patient does not need help and his heartbeat is okay check for actions
    if (patient.action[0].type === '') {
      return { backgroundColor: 'rgba(33,186,69,0.94)'
      }
    }
    if (patient.action[0].done === true) {
      return { backgroundColor: 'rgba(33,186,69,0.94)'
      }
    } else {
      let now = new Date()
      let action = new Date(patient.action[0].time)
      console.log(now - action)
      if (action - now < 0) {
        return { backgroundColor: 'rgba(224, 147, 2, 0.94)'
        }
      }
      if (action - now > 0 && action - now < actionTime) {
        return { backgroundColor: 'rgba(255, 187, 0, 0.74)'
        }
      } else {
        return { backgroundColor: 'rgba(33,186,69,0.94)' }
      }
    }
  }
}
