
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'department/:dep_name/:dep_nr', name: 'department', component: () => import('pages/Department'), props: true },
      { path: 'department/:dep_name/room/:rm_nr', name: 'patient', component: () => import('pages/Room'), props: true },
      { path: 'info', component: () => import('pages/Info.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
